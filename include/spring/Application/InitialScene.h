#pragma once

#include "spring\Framework\IScene.h"
#include "qobject.h"
#include<qpushbutton.h>
#include<qstring.h>
#include<qlineedit.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:
		InitialScene(const std::string& ac_szSceneName);

		virtual void createScene();

		virtual void release();

		~InitialScene();
	signals:
		void pressed(); 
	
	public slots:
		void buttonPressed();
	private:

		QWidget *centralWidget;
	public: 
		static const QString contentName;//store the content of the lineEdit -  our text
	
	};
}