#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include<spring\Application\InitialScene.h>



namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
	public slots:
		void buttonPressed();
	signals:
		void pressed();
	private:
		QWidget * centralWidget;


	};
}
