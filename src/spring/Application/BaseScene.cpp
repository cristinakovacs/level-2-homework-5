#include <spring\Application\BaseScene.h>
#include<qstring.h>
#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
	
		std::shared_ptr <Ui_MainWindow> mySharedPointer = std::make_shared <Ui_MainWindow>();
		mySharedPointer->setupUi(m_uMainWindow.get());
		m_uMainWindow->setWindowTitle(QString("Hello!"+InitialScene::contentName));//set the name of the scene
		mySharedPointer->label->setText(QString("Hello!" + InitialScene::contentName));//set the lable content - we take the content of the lineEdit from the first scene and put it on the second scene
		/*std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);
		m_uMainWindow->setWindowTitle(QString(appName.c_str()));*/ 
		//another way to set the title of the scene
		connect(ui->pushButton, SIGNAL(pressed()), this, SLOT(buttonPressed()));//one signal: if the buttton is pressed, we handle the event

	}
	void BaseScene::release()
	{
		delete centralWidget;
	
	}

	BaseScene::~BaseScene()
	{

	}
	void BaseScene::buttonPressed()//we have the option to go back to the first scene
	{
		
		const std::string c_szNextSceneName = "Initial Scene";//choose the name of the scene - Initial Scene
		emit SceneChange(c_szNextSceneName);//move to the next scene; we don't have an event for "previous scene", but we can use the same method
	}
}
