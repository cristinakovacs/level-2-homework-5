#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = std::string("Initial scene");
const std::string baseSceneName = std::string("Base scene");

namespace Spring
{
	ApplicationModel::ApplicationModel():IApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		m_initialScene = std::make_shared<InitialScene>(initialSceneName);
		m_secondScene = std::make_shared<BaseScene>(baseSceneName);

		m_Scenes.emplace(initialSceneName, m_initialScene.get());
		m_Scenes.emplace(baseSceneName, m_secondScene.get());
	}
	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void ApplicationModel::defineTransientData()
	{
		std::string name = "";
		m_TransientData.emplace("Name", name);
	}
}
