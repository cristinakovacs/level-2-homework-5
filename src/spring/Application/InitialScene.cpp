#include <spring/Application/InitialScene.h>
#include "ui_initial_scene.h"
#include<qlineedit.h>
namespace Spring
{

	InitialScene::InitialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void InitialScene::createScene()
	{

		std::shared_ptr <Ui_MainWindow> mySharedPointer = std::make_shared <Ui_MainWindow>();
		mySharedPointer->setupUi(m_uMainWindow.get());
		m_uMainWindow->setWindowTitle(QString("Say Hello!"));//set the title of the scene
		mySharedPointer->lineEdit->text.setText("");//this field is empty - we will edit it
		connect(ui->pushButton, SIGNAL(pressed()), this, SLOT(buttonPressed()));//one signal: if the buttton is pressed, we handle the event
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
		//delete contentName;
	}

	void InitialScene::buttonPressed()
	{
		contentName = ui->lineEdit.text;//get the content of the lineEdit - our name
		const std::string c_szNextSceneName = "Base scene";//we choose the name of the next scene; it has to be predefined
		emit SceneChange(c_szNextSceneName);//move to the next scene
	}

}
